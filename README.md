COMP1005 2022 Tutorial 9
========================


## Background
* [Recursion: Part A](https://www.learn-c.org/en/Recursion)  
* [Recursion: Part B](https://www.tutorialspoint.com/cprogramming/c_recursion.htm)  
* [Linear Search](https://www.tutorialspoint.com/data_structures_algorithms/linear_search_algorithm.htm)  
* [Binary Search](https://www.tutorialspoint.com/data_structures_algorithms/binary_search_algorithm.htm)
* [Interpolation Search](https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm)  
* [Hash Table](https://www.tutorialspoint.com/data_structures_algorithms/hash_data_structure.htm)

## Exercises
0. Write a program in C to calculate the sum of numbers from 1 to n using recursion.  
    <img src="https://www.w3resource.com/w3r_images/c-recursion-image-exercise-2.png" width="128">  
    ***Expected Output:*** 
    ```
    Input the value of n : 5

    The sum of numbers from 1 to 5:
    15
    ```

0. Write a C program using binary search to find the position (index) of the target number in some user-given numbers.  
    <img src="https://www.w3resource.com/w3r_images/binary-search-1.png" width="192">  
    <img src="https://www.w3resource.com/w3r_images/binary-search-2.png" width="192">  
    <img src="https://www.w3resource.com/w3r_images/binary-search-3.png" width="192">  
    ***Expected Output:*** 
    ```
    Input  no. of elements in an array: 3  
    Input 3 values in ascending order: 
    15
    18
    20

    Input the value to search: 15
    15 was found at position 0.
    ```

## Extras
0. Write a program in C to find the sum of digits of a number using recursion.  
    <img src="https://www.w3resource.com/w3r_images/c-recursion-image-exercise-6.png" width="128">  
    ***Expected Output:*** 
    ```
    Input any number to find sum of digits: 25 

    The Sum of digits of 25 is: 7
    ```

0. Write a program in C to convert a decimal number to binary using recursion.  
    <img src="https://www.w3resource.com/w3r_images/c-recursion-image-exercise-11.png" width="128">  
    ***Expected Output:*** 
    ```
    Input any decimal number : 66 

    The Binary value of decimal no. 66 is : 1000010
    ```

0. Try other search algorithms.  
